import { heroes } from "./data/heroes";

const getHeroeByIdAsync = (id) => (

    new Promise((resolve, reject) => {
        setTimeout(() => {
            const heroe = heroes.find(heroe => heroe.id === id);
            heroe ? resolve(heroe) : reject('Heroe not found');
        }, 2000);
    })
);



getHeroeByIdAsync(5).then(console.log).catch(console.warn);



